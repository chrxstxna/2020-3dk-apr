import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDataAccessObject {


    private DatabaseAccess databaseAccess;

    public MessageDataAccessObject(String host, String port, String database, String user, String password, String dbVendor) {
        try {
            databaseAccess = new DatabaseAccess(host, port, database, user, password, dbVendor);
        } catch(DbVendorNotSuppertedException e){
            System.err.println("Could not start program!");
            System.exit(1);
        }
    }
    public List<Message> readAllMessages(){
        Connection connection = connect();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = "select * from message";
            ResultSet resultSet;
            resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while(resultSet.next()){
                messages.add(new Message(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3)));
            }

            return messages;
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment ", throwables);
        }finally {
            close(connection);
        }
    }

   public Message read(int id){
        Message message = null;
        Connection connection = connect();
        Statement statement = null;
        String sql = "select * from message where id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
            message = new Message(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3));}
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
        }
        return message;
    }

    /*public void update (int id, String text, String language){
        //String sql = "update message set text = '"+message.getMessage()+"', language = '"+message.getLanguage()+"' Where id="+message.getId();
        Connection connection = connect();
        String sql = "update message set text = '"+text+"', language = '"+language+"' where id = " +id;
        Statement update = null;
        try{
            update = connection.createStatement();
            update.execute(sql);
            System.out.println("Update executed successfully");

        } catch(SQLException throwables){
            throw new RuntimeException("Could not update statment ", throwables);
        }finally {
            close(connection);
        }
     }*/
    public boolean safeupdate (int id, String text, String language){
        Connection connection = connect();
        String sql = "update message set text = ?, language = ? where id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,text);
            preparedStatement.setString(2,language);
            preparedStatement.setInt(3,id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }return false;

        } catch(SQLException throwables){
            throw new RuntimeException("Could not update statment safe", throwables);
        }finally {
            close(connection);
        }
    }



     /*public void delete (int id){
         Connection connection = connect();
         String sql = "delete from message where id = " +id;
         Statement delete = null;
         try{
             delete = connection.createStatement();
             delete.execute(sql);
             System.out.println("Deleted successfully");

         } catch(SQLException throwables){
             throw new RuntimeException("Could not delete statment ", throwables);
         }finally {
             close(connection);
         }
     }*/

    public void safedelete(int id){
        Connection connection = connect();
        String sql = "delete from message where id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);

        } catch(SQLException throwables){
            throw new RuntimeException("Could not delete statment safe", throwables);
        }finally {
            close(connection);
        }
    }



    public void insert(String text, String language){
        Connection connection = connect();
        String sql = "insert into message(text, language) VALUES ('"+ text+ "','" + language+ "')";
        Statement insert = null;
        try {
            insert = connection.createStatement();
            insert.execute(sql);
            System.out.println("Insert executed successfully");
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statement ", throwables);
        } finally{
            close(connection);
        }

        System.out.println("Insert executed succesfully: ");
    }

    private Connection connect(){
        return databaseAccess.connect();
    }

    private void close(Connection connection){
        databaseAccess.close(connection);
    }

}
