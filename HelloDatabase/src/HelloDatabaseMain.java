import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HelloDatabaseMain {
    public static void main(String[] args){

        String dbVendor = null;
        if(args.length == 1 && args[0].equals("oracle")){
            dbVendor = "oracle";
        } else {
            dbVendor ="mariadb"; //use "postgress" to get an error
        }

        MessageDataAccessObject messageDAO = new MessageDataAccessObject("localhost","3306","helloworld","root","", dbVendor);
        List<Message> messages = messageDAO.readAllMessages();
        //messageDAO.insert("Bonjour le monde ","fr");
        messages.forEach(System.out::println);

        Message msg = messageDAO.read(2);
        System.out.println(msg);

        messageDAO.safeupdate(3,"konnichiwa sekai", "ja");
        messageDAO.readAllMessages();
        //messageDAO.safedelete(3);


    }
}


