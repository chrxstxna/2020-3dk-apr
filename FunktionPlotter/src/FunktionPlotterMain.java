import javax.swing.*;

public class FunktionPlotterMain {
    public static void main(String[] args) {
        FunktionPlotter plotter = new FunktionPlotter("Funktionsplotter");
        plotter.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        plotter.setVisible(true);
        plotter.setSize(600,550);
    }
}
