import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginMask extends JFrame {
    public LoginMask(String title)  {
        super(title);
        setLayout(new BorderLayout());
        UserDataAccessObject userDao = new UserDataAccessObject("localhost","3306","login_gui_db","root","");

        JPanel structure = new JPanel();
        structure.setLayout(new GridLayout(4,2,20,20));

        JLabel username = new JLabel("Benutzername: ");

        JTextField user = new JTextField();
        

        JLabel passwordfield = new JLabel("Passwort: ");

        JPasswordField password = new JPasswordField();


        JButton login = new JButton("Login");
        login.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("login: "+ userDao.login(user.getText(), new String(password.getPassword())));
                System.out.println("securelogin: "+ userDao.secureLogin(user.getText(), new String(password.getPassword())));
            }
        });

        JButton register = new JButton("Registrieren");

        JButton forgottenpw = new JButton("Passwort vergessen");

        structure.add(username);
        structure.add(user);
        structure.add(passwordfield);
        structure.add(password);
        structure.add(login);
        structure.add(register);
        structure.add(forgottenpw);
        add(structure);

    }
}
