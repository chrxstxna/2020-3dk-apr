import java.sql.*;

public class UserDataAccessObject {

    private final String connectionURL;
    private final String username;
    private final String password;

    public UserDataAccessObject(String host,String port, String databasename, String username, String password) {
        this.connectionURL = "jdbc:mariadb://"+host+":"+port+"/"+databasename;
        this.username = username;
        this.password = password;
        try{
            Class.forName("org.mariadb.jdbc.Driver");
        }catch (ClassNotFoundException e){
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    private Connection connect(){
        try {
            return DriverManager.getConnection(this.connectionURL, this.username, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection");
        }
    }

    public boolean login(String username, String password){
        Connection connection = connect();
        String sql = "select * from user where username = '"+username+"' and password = '"+password+"'";
        Statement login = null;
        try {
            login = connection.createStatement();
            ResultSet resultSet = login.executeQuery(sql);
            if(resultSet.first()){
                return true;
            }
            return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run login query", throwables);
        } finally {
            close(connection);
        }
    }

    public boolean secureLogin(String username, String password){
        Connection connection = connect();
        String sql = "select * from user where username = ? and password = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run preparedStatement");
        }finally {
            close(connection);
        }
    }
}
