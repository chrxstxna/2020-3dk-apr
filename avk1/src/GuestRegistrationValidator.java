import java.util.ArrayList;

// AVK3: Gute Arbeit
// 11 Punkte

public class GuestRegistrationValidator {

    GuestRegistrationGUI gui = new GuestRegistrationGUI("Guest Registrition"); // braucht man nicht

    // der Name dieser Methode ist schlecht, sollte validate sein (siehe Angabe)
    public void throwExceptions(String firstname, String lastname, String email, String tablenumber, String phonenumber) throws GuestDataNotValidException {
        ArrayList<String> exceptions = new ArrayList<String>();
        try{
            checkfirstName(firstname,15);
        }catch(IllegalArgumentException a){
            exceptions.add(a.getMessage());
        }

        try{
            checklastName(lastname,20);
        }catch(IllegalArgumentException b){
            exceptions.add(b.getMessage());
        }

        try{
            combination(firstname, lastname);
        }catch(IllegalArgumentException c) {
            exceptions.add(c.getMessage());
        }

        try{
            oneOrBoth(email, phonenumber);
        }catch(IllegalArgumentException d){
            exceptions.add(d.getMessage());
        }

        try{
            checktableNumber(tablenumber);
        }catch(IllegalArgumentException e){
            exceptions.add(e.getMessage());
        }

        if(!exceptions.isEmpty()){
            throw new GuestDataNotValidException(exceptions);
        }
    }


    private void checkfirstName(String firstname, int number){
        // hier hätte ich isEmpty() bzw. maxLength-Methoden erwartet (siehe Angabe): -1
        if(firstname.isEmpty()|| firstname.length() > number){
            throw new IllegalArgumentException("First name must not be empty or longer than "+ number+" characters");
        }
    }
    private void checklastName(String lastname, int number){
        if(lastname.isEmpty()|| lastname.length() > number){
            throw new IllegalArgumentException("Last name must not be empty or longer than "+ number+" characters");
        }
    }
    private void combination(String firstname, String lastname){
        if(firstname.equals("Max") && lastname.equals("Mustermann")){
            throw new IllegalArgumentException("Max Mustermann ist not allowed");
        }
    }

    private void oneOrBoth(String email, String phonenumber){
        if(email.isEmpty() && phonenumber.isEmpty()){
            throw new IllegalArgumentException("Email or phone number must be provided");
        }
    }

    private void checktableNumber(String number){
        try {
            int tableNumber = Integer.parseInt(number);
            if(tableNumber > 100 || tableNumber < 0){
                throw new IllegalArgumentException("Table number must not be empty");
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Table number must not be empty");
        }
    }



}
