import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuestRegistrationGUI extends JFrame {
    private JTextField firstNameTextField;
    private JTextField lastNameTextField;
    private JTextField streetTextField;
    private JTextField plzTextField;
    private JTextField placeTextField;
    private JTextField telefonNumberTextField;
    private JTextField eMailTextField;
    private JTextField tableNumberTextField;
    public GuestRegistrationGUI(String title) {
        setPreferredSize(new Dimension(640,275));
        setLayout(null); // Layoutmanagement war gefordert: -2

        JLabel headline = new JLabel("Gästeregistrierung");
        headline.setBounds(230,10,250,30);
        headline.setFont(new Font("Arial",Font.BOLD,22));

        JLabel firstName = new JLabel("Vorname:");
        firstName.setBounds(10,60,100,20);

        JLabel lastName = new JLabel("Nachname:");
        lastName.setBounds(10,85,100,20);

        JLabel street = new JLabel("Straße:");
        street.setBounds(10,110,100,20);

        JLabel plz = new JLabel("PLZ:");
        plz.setBounds(10,135,100,20);

        JLabel place = new JLabel("Ort:");
        place.setBounds(10,160,100,20);


        firstNameTextField = new JTextField();
        firstNameTextField.setBounds(140,60,140,20);

        lastNameTextField = new JTextField();
        lastNameTextField.setBounds(140,85,140,20);

        streetTextField = new JTextField();
        streetTextField.setBounds(140,110,140,20);

        plzTextField = new JTextField();
        plzTextField.setBounds(140, 135,50,20);

        placeTextField = new JTextField();
        placeTextField.setBounds(140,160,140,20);

        JLabel telefonnumber = new JLabel("Telefonnummer:");
        telefonnumber.setBounds(360,60,100,20);

        JLabel eMail = new JLabel("E-Mail:");
        eMail.setBounds(360,85,100,20);

        JLabel tableNumber = new JLabel("Tisch-Nr:");
        tableNumber.setBounds(360,110,100,20);

        telefonNumberTextField = new JTextField();
        telefonNumberTextField.setBounds(480,60,140,20);

        eMailTextField = new JTextField();
        eMailTextField.setBounds(480,85,140,20);

        tableNumberTextField = new JTextField();
        tableNumberTextField.setBounds(480,110,140,20);


        JButton save = new JButton("Speichern");
        save.setBounds(200,200,100,25);
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuestRegistrationValidator validator = new GuestRegistrationValidator();
                try {
                    validator.throwExceptions(firstNameTextField.getText(), lastNameTextField.getText(), eMailTextField.getText(), tableNumberTextField.getText(), telefonNumberTextField.getText());
                } catch (GuestDataNotValidException guestDataNotValidException) {
                    System.err.println(guestDataNotValidException.getMessages());
                }
            }
        });

        JButton reset = new JButton("Zurücksetzen");
        reset.setBounds(310,200,120,25);


        add(headline);
        add(firstName);
        add(firstNameTextField);
        add(lastName);
        add(lastNameTextField);
        add(street);
        add(streetTextField);
        add(plz);
        add(plzTextField);
        add(place);
        add(placeTextField);
        add(telefonnumber);
        add(telefonNumberTextField);
        add(eMail);
        add(eMailTextField);
        add(tableNumber);
        add(tableNumberTextField);
        add(save);
        add(reset);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        pack();
    }

}
