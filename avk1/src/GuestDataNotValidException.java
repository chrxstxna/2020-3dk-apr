import java.util.ArrayList;

public class GuestDataNotValidException extends Exception {
    private String messages = "";

    public GuestDataNotValidException(ArrayList<String> messages) {
        for(String s : messages){
            this.messages += s + "\n";
        }

    }

    public String getMessages() {
        return messages;
    }
}




