public class MyThread extends Thread {
    private String name;
    private int sleeptime; //in milliseconds

    public MyThread(String name, int sleeptime) {
        this.name = name;
        this.sleeptime = sleeptime;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(this.sleeptime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Mein Name ist "+name);
    }
}
