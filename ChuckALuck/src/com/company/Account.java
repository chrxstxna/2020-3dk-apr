package com.company;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Account extends JPanel {
    public JLabel lblScore;
    public JButton btnAddStake;


    public Account()
    {
        setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
        setBorder(new EmptyBorder(6,10,10,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblAccount = new JLabel("Konto");
        lblAccount.setHorizontalAlignment(SwingConstants.CENTER);
        lblAccount.setPreferredSize(new Dimension(120,30));
        lblAccount.setOpaque(true);
        lblAccount.setBackground(Color.WHITE);
        add(lblAccount);

        lblScore = new JLabel();
        lblScore.setText("0");
        lblScore.setHorizontalAlignment(SwingConstants.CENTER);
        lblScore.setPreferredSize(new Dimension(30,30));
        lblScore.setOpaque(true);
        lblScore.setBackground(Color.WHITE);
        add(lblScore);

        btnAddStake = new JButton("Einsatz zahlen");
        btnAddStake.setPreferredSize(new Dimension(120,30));

        add(btnAddStake);
    }
    public int getKontostand() {
        return Integer.parseInt( lblScore.getText());
    }

    public void setKontostand(int kontostand) {
        lblScore.setText(Integer.toString(kontostand));
    }
}