package com.company;

import javax.swing.*;

public class ChuckALuckGame {

    public void setMoney(JLabel label, JButton button, JButton diceButton) {
        label.setText("5");
        button.setEnabled(false);
        diceButton.setEnabled(true);

    }

    public int throwDice(JButton button, int kontostand, int setNumber, ThrowDice dice) {
        if (kontostand > 0) {

            int firstNumber = (int) (Math.random()*6 +1);
            int secondNumber = (int) (Math.random()*6 +1);
            int thirdNumber = (int) (Math.random()*6 +1);

            dice.lblDice1.setText(Integer.toString(firstNumber));
            dice.lblDice2.setText(Integer.toString(secondNumber));
            dice.lblDice3.setText(Integer.toString(thirdNumber));

            if (firstNumber == setNumber) {
                kontostand += 1;
            }
            if (secondNumber == setNumber) {
                kontostand += 1;
            }
            if (thirdNumber == setNumber) {
                kontostand += 1;
            } else if ((firstNumber != setNumber) && (secondNumber != setNumber)) {
                kontostand -= 1;
            }

            if (kontostand == 0) {
                dice.btnDice.setEnabled(false);
                button.setEnabled(true);

            }
        }
        return kontostand;

    }

}
