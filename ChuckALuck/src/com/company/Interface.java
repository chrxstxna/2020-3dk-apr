package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interface extends JFrame {
    private final String[] ARR = new String[]{"lblDice1", "lblDice2","lblDice3"};

    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");

    private Account myAccount = new Account();
    private SetNumber myNumber = new SetNumber();
    private ThrowDice myDice = new ThrowDice();
    private ChuckALuckGame chuckALuckGame = new ChuckALuckGame();

    public Interface(String title) {
        super(title);
        add(myAccount, BorderLayout.WEST);
        add(myNumber, BorderLayout.CENTER);
        add(myDice, BorderLayout.EAST);

        heading.setBackground(Color.BLUE);

        heading.add(lblHeading);
        lblHeading.setForeground(Color.WHITE);
        lblHeading.setFont(new Font("Times New Roman", Font.BOLD, 18));
        lblHeading.setHorizontalAlignment(SwingConstants.CENTER);
        add(heading, BorderLayout.NORTH);

        myAccount.btnAddStake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chuckALuckGame.setMoney(myAccount.lblScore, myAccount.btnAddStake, myDice.btnDice);
            }
        });

        myDice.btnDice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myAccount.setKontostand(chuckALuckGame.throwDice(myAccount.btnAddStake,myAccount.getKontostand(), myNumber.getNumber(), myDice));
                myNumber.resetNumber();
            }
        });

    }
}
