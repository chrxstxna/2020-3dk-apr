import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Buttons extends JPanel {

    private final ArticleDataAccessObject articleDao;

    private JTextField articleNumberTextfield;
    private  JTextField articleDescriptionTextfield;
    private JTextField articlePriceTextfield;
    private JTextField articleStockTextfield;


    public Buttons(ArticleDataAccessObject articleDAO, ArticleTableModel tableModel) {

        this.articleDao = articleDAO;
        setPreferredSize(new Dimension(500, 200));

        setLayout(null);

        JLabel artikel = new JLabel("Artikel");
        artikel.setBounds(10, 10, 70, 20);
        artikel.setFont(new Font("Arial", Font.BOLD, 18));

        JLabel artikelnummer = new JLabel("Artikelnummer:");
        artikelnummer.setBounds(10, 50, 120, 20);

        JLabel artikelbezeichnung = new JLabel("Artikelbezeichnung:");
        artikelbezeichnung.setBounds(10, 80, 120, 20);

        JLabel verkaufspreis = new JLabel("Verkaufspreis:");
        verkaufspreis.setBounds(10, 110, 120, 20);

        JLabel bestand = new JLabel("Bestand:");
        bestand.setBounds(10, 140, 120, 20);


        articleNumberTextfield = new JTextField();
        articleNumberTextfield.setEditable(false);
        articleNumberTextfield.setBounds(150, 50, 200, 20);

        articleDescriptionTextfield = new JTextField();
        articleDescriptionTextfield.setBounds(150, 80, 200, 20);

        articlePriceTextfield = new JTextField();
        articlePriceTextfield.setBounds(150, 110, 200, 20);

        articleStockTextfield = new JTextField();
        articleStockTextfield.setBounds(150, 140, 200, 20);

        JButton neu = new JButton("Neu");
        neu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetFields();
            }
        });
        neu.setBounds(390, 10, 100, 20);

        ArticleDatabase database =  new ArticleDatabase();

        JButton anlegen = new JButton("Anlegen");
        anlegen.setBounds(390, 50, 100, 20);
        anlegen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Connection connection = null;

                String articledesc = checkEmpty(articleDescriptionTextfield.getText());
                String price = checkEmpty(articlePriceTextfield.getText());
                String stockField = checkEmpty(articleStockTextfield.getText());

                String description = checkMaxLength(articledesc,15);
                double articlePreis = checkDouble(price);
                int stock = checkInteger(stockField);
                Article a = new Article(0, description, articlePreis, stock);
                articleDAO.insert(0,description,articlePreis,stock);
                List<Article> articles = articleDAO.readAllArticles();
                articles.forEach(System.out :: println);
                tableModel.initData(articles);
            }
        });

        JButton aendern = new JButton("Ändern");
        aendern.setBounds(390, 80, 100, 20);
        aendern.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = Integer.parseInt(articleNumberTextfield.getText());

                String articledesc = checkEmpty(articleDescriptionTextfield.getText());
                String price = checkEmpty(articlePriceTextfield.getText());
                String stockField = checkEmpty(articleStockTextfield.getText());

                String description = checkMaxLength(articledesc,20);
                double articlePreis = checkDouble(price);
                int stock = checkInteger(stockField);
                Article a = new Article(id, description, articlePreis, stock);
                articleDAO.update(a);
                List<Article> articles = articleDAO.readAllArticles();
                articles.forEach(System.out :: println);
                tableModel.initData(articles);
            }
        });

        JButton loeschen = new JButton("Löschen");
        loeschen.setBounds(390, 110, 100, 20);
        loeschen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int articleID = checkInteger(articleNumberTextfield.getText());
                articleDAO.delete(articleID);
                List<Article> articles = articleDAO.readAllArticles();
                tableModel.initData(articles);
                resetFields();
            }
        });

        JButton suchen = new JButton("Suchen");
        suchen.setBounds(390, 140, 100, 20);
        suchen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String desc = articleDescriptionTextfield.getText();
                List <Article> foundArticles = articleDao.findBy(desc);
                tableModel.initData(foundArticles);
            }
        });


        add(artikel);
        add(neu);
        add(artikelnummer);
        add(articleNumberTextfield);
        add(anlegen);
        add(artikelbezeichnung);
        add(articleDescriptionTextfield);
        add(aendern);
        add(verkaufspreis);
        add(articlePriceTextfield);
        add(loeschen);
        add(bestand);
        add(articleStockTextfield);
        add(suchen);

    }


    private String checkEmpty(String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Eingabe darf nicht leer sein!!");
        } else {
            return value;
        }
    }

    private String checkMaxLength(String value, int maxLength){
        if(value.length() > maxLength){
            throw new IllegalArgumentException("article name must have less than "+maxLength+" characters");
        }else {
            return value;
        }

    }

    private int checkInteger(String value){
        try{
            return Integer.parseInt(value);
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("value "+value+" must be a numeric value");
        }
    }

    private double checkDouble(String value){
        try{
            return Double.parseDouble(value);
        }catch(NumberFormatException e){
            throw new IllegalArgumentException("value "+value+" must be a decimal value");
        }
    }

    public void loadArticle(int id){
        Article article = articleDao.read(id);
        articleNumberTextfield.setText(article.getId()+"");
        articleDescriptionTextfield.setText(article.getDescription()+"");
        articlePriceTextfield.setText(article.getPrice()+"");
        articleStockTextfield.setText(article.getStock()+"");

    }

    private void resetFields(){
        this.articleNumberTextfield.setText("");
        this.articleDescriptionTextfield.setText("");
        this.articlePriceTextfield.setText("");
        this.articleStockTextfield.setText("");
    }


}

