import java.util.HashMap;

public class ArticleDatabase {
    private HashMap<Integer, Article> database;

    public ArticleDatabase() {
        this.database = new HashMap<>();
    }

    public void insert(Article article) throws ArticleAlreadyExistsException {
        if(database.containsKey(article.getId())){
            throw new ArticleAlreadyExistsException(article.getId());
        }
        this.database.put(article.getId(), article);
    }



    @Override
    public String toString() {
        return "ArticleDatabase{" +
                "database=" + database +
                '}';
    }

    public void delete (int articleID){
        if(database.containsKey(articleID)){
            this.database.remove(articleID);
        }else{
            throw new ArticleDoesNotExistException(articleID);
        }
    }
}
