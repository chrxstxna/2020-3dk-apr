import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Textfield extends JPanel {



    public Textfield(ArticleTableModel tableModel, Buttons buttons, ArticleDataAccessObject articleDAO) {


        JTable table = new JTable(tableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSelectionModel = table.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ArticleListSelectionListener(table, buttons));



        setPreferredSize(new Dimension(500,250));

        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);


        JTextField textField = new JTextField();
        textField.setBounds(10,10,500,200);
        textField.setBorder(new LineBorder(Color.BLACK));

        scrollPane.setBounds(10,10,500,200);


        add(scrollPane,BorderLayout.CENTER);

        JPanel button = new JPanel();
        button.setLayout(new BorderLayout());
        add(button,BorderLayout.SOUTH);

        JButton showArticles = new JButton("Zeige Artikelliste");
        showArticles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                tableModel.initData(articleDAO.readAllArticles());
            }
        });
        JButton end = new JButton("Beenden");
        end.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        button.add(showArticles,BorderLayout.WEST);
        button.add(end,BorderLayout.EAST);
    }
}
