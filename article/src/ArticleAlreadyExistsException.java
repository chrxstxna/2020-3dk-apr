public class ArticleAlreadyExistsException extends Exception{
    public ArticleAlreadyExistsException(int articleID) {
        super("Article with ID "+articleID+" already exists");

    }
}
