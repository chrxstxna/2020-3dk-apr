import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ArticleListSelectionListener implements ListSelectionListener {

    private JTable table;
    private Buttons buttons;

    public ArticleListSelectionListener(JTable table, Buttons buttons){
        this.table = table;
        this.buttons = buttons;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
        return;
        }

        ListSelectionModel lsn = (ListSelectionModel) e.getSource();
        int selectedRow =  lsn.getMinSelectionIndex();
        if(selectedRow >0) {
            int id = (int) table.getValueAt(selectedRow,0);
            System.out.println("Selected id: "+id);

            buttons.loadArticle(id);
            System.out.println("Row " + selectedRow + " selected");
        }






    }
}
