import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class Window extends JFrame {

    public Window(String title) throws IOException {
        super(title);
        Properties properties = new Properties();
        FileInputStream in = new FileInputStream("database.properties");
        properties.load(in);
        in.close();

        ArticleDataAccessObject articleDao = new ArticleDataAccessObject(properties.get("host"),properties.get("port"), properties.get("database"),properties.get("user"),properties.get("password"));

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setSize(700,700);

        ArticleTableModel tableModel = new ArticleTableModel();
        tableModel.initData(articleDao.readAllArticles());



        Buttons buttons = new Buttons(articleDao, tableModel);
        Textfield textfield = new Textfield(tableModel, buttons, articleDao);

        setContentPane(contentPane);
        contentPane.add(buttons,BorderLayout.NORTH);
        contentPane.add(textfield,BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,700);
        setVisible(true);
        pack();
    }
}
