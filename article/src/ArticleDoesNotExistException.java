public class ArticleDoesNotExistException extends RuntimeException{
    public ArticleDoesNotExistException(int articleId){
        super("Article with id "+articleId+" does not exst");
    }
}