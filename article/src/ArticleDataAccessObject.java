import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDataAccessObject {
    private final String user;
    private final String password;
    private final String databaseURL;

    public ArticleDataAccessObject(Object host, Object port, Object database, Object user, Object password) {
        this.user = user.toString();
        this.password = password.toString();
        this.databaseURL = "jdbc:mariadb://"+host+":"+port+"/"+database;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }


    public Article read(int id){
        Connection connection = connect();
        String sql = "select * from article where id =" +id;
        Statement select = null;
        try{
            select = connection.createStatement();
            ResultSet resultSet = select.executeQuery(sql);
            List<Article> articles = extractArticles(resultSet);
            return articles.get(0);
    }catch (SQLException throwables){
            throw new RuntimeException("Could not create statment ", throwables);
        }finally {
            close(connection);
        }
    }

    public List<Article> readAllArticles(){
        Connection connection = connect();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = "select * from article";
            ResultSet resultSet;
            resultSet = statement.executeQuery(sql);
            return extractArticles(resultSet);
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment ", throwables);
        }finally {
            close(connection);
        }
    }

    public void insert(int id, String description, double price, int stock){
        Connection connection = connect();
        String sql = "insert into article(id, description, price, stock) VALUES ('"+id+"','"+ description+ "','" + price+ "','"+stock+"')";
        Statement insert = null;
        try {
            insert = connection.createStatement();
            insert.execute(sql);
            System.out.println("Insert executed successfully");
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statement ", throwables);
        } finally{
            close(connection);
        }

        System.out.println("Insert executed succesfully: ");
    }

    public void update(Article article){
        Connection connection = connect();
        String sql = "update article set description = '"+article.getDescription()+"', price = '"+article.getPrice()+"', stock = '"+article.getStock()+"' where id = "+article.getId();
        Statement update = null;
        try {
            update = connection.createStatement();
            update.execute(sql);
            System.out.println("update executed successfully");
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(connection);
        }

    }

    public List<Article> findBy (String description){
        Connection connection = connect();
        String sql = "Select * from article where description like '%"+description+"%'";
        Statement findBy = null;
        ArrayList<Article> articles = new ArrayList<>();
        try {
            findBy = connection.createStatement();
            ResultSet resultSet = findBy.executeQuery(sql);
            return extractArticles(resultSet);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statment ", throwables);
        }finally {
            close(connection);
        }
    }

    private List<Article> extractArticles(ResultSet resultSet) throws SQLException {
        ArrayList<Article> articles = new ArrayList<>() ;
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String desc = resultSet.getString(2);
            Double prc = resultSet.getDouble(3);
            int stck = resultSet.getInt(4);
            articles.add(new Article(id, desc, prc, stck));
        }
        return articles;
    }

    public void delete(int id){
        Connection connection = connect();
        String sql = "delete from article where id = " +id;
        Statement delete = null;
        try{
            delete = connection.createStatement();
            delete.execute(sql);
            System.out.println("Deleted successfully");

        } catch(SQLException throwables){
            throw new RuntimeException("Could not delete statment ", throwables);
        }finally {
            close(connection);
        }
    }

    private Connection connect(){
        try {
            return DriverManager.getConnection(this.databaseURL, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
