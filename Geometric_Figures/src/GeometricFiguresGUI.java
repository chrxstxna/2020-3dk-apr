import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Flow;

public class GeometricFiguresGUI extends JFrame {
    private String mode;
    public GeometricFiguresGUI(){

        super("Geometrische Figuren Zeichnen");

        Canvas drawingArea= new Canvas(this);
        drawingArea.addMouseListener(new MouseClickHandler(drawingArea));

        JPanel radioButtonPanel = new JPanel();
        radioButtonPanel.setPreferredSize(new Dimension(75,550));


        JRadioButton square = new JRadioButton("Rechteck");
        square.addActionListener(new ModeSettingActionListener(this));
        JRadioButton circle = new JRadioButton("Kreis");
        circle.addActionListener(new ModeSettingActionListener(this));
        JRadioButton disc = new JRadioButton("Scheibe");
        disc.addActionListener(new ModeSettingActionListener(this));

        ButtonGroup group = new ButtonGroup();
        group.add(square);
        group.add(circle);
        group.add(disc);

        radioButtonPanel.add(square);
        radioButtonPanel.add(circle);
        radioButtonPanel.add(disc);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new FlowLayout());
        contentPane.setSize(600,550);

        contentPane.add(drawingArea);
        contentPane.add(radioButtonPanel);

        setContentPane(contentPane);


        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,600);
        setVisible(true);
        pack();
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }
}
