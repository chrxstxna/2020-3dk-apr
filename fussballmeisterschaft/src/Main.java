import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
    ChampionshipDataAccessObject cdao = new ChampionshipDataAccessObject("localhost","3306","fussballmeisterschaft","root", "");
    Championship meisterschaft = cdao.loadForLeague("1. Klasse Waldviertel");
    //print(meisterschaft);

    //gameDays(meisterschaft);

        List<String> teams = new ArrayList<>();
        /*for(Team t : meisterschaft.getTeam()){
            teams.add(t.getName());
        }*/

        teams.add("1");
        teams.add("2");
        teams.add("3");
        teams.add("4");
        teams.add("5");
        teams.add("6");
        teams.add("7");
        teams.add("8");


        String joker = teams.remove(teams.size()-1);
        ArrayList games = new ArrayList();
        GameDataAccessObject gdao = new GameDataAccessObject(cdao);

        for (int i = 0; i < teams.size(); i++) {
            System.out.println("Spieltag "+(i+1)+":");
            int j = 0;
            while(j < teams.size()/2){
                System.out.println(teams.get(j)+" : "+ teams.get(teams.size()-1-j));
                games.add(new Game(meisterschaft.getTeam().get(Integer.parseInt(teams.get(j))-1),meisterschaft.getTeam().get(teams.size()-1-j),new Date(2021-1900,6,10+j)));
                j ++;
            }
            System.out.println(teams.get(j)+" : "+joker);
            games.add(new Game(meisterschaft.getTeam().get(Integer.parseInt(teams.get(j))-1),meisterschaft.getTeam().get(Integer.parseInt(joker)-1),new Date(2021-1900,6,15)));
            String t = teams.remove(0);
            teams.add(t);

            System.out.println("-------------------");
        }
        gdao.insert(games);


    //games = createTestgames(meisterschaft);

    /*Game g1 = new Game(meisterschaft.getTeam().get(2),meisterschaft.getTeam().get(1),new Date(2021,6,15));
    Game g2 = new Game(meisterschaft.getTeam().get(3),meisterschaft.getTeam().get(4),new Date(2021,6,15));
    games.add(0,g1);
    games.add(1,g2);
    gdao.insert(games);*/



    }

    private static void print(Championship championship){
        String headline = championship.getLeague().getName() + " ("+ championship.getLeague().getYear() + ")";
        System.out.println(headline);
        int len = headline.length();
        for(int i = 0; i < len; i++){
            System.out.print("-");
        }
        System.out.println();

        for(Team t : championship.getTeam()){
            System.out.println(" "+t.getName());
        }
    }

    private static void gameDays(Championship championship) {
        ArrayList<String> teams = new ArrayList<String>();
        for (int i = 1; i < 6+1; i++) {
            teams.add("" + i);
        }
        /*for(Team t : championship.getTeam()){
            teams.add(t.getName());
        }*/
        int length = teams.toArray().length;
        int lengthList = length - 1;

        //Spieltage anhand der Teams -Liste ausgeben
        for (int i = 1; i < length; i++) {
            System.out.println("Spieltag " + i + ":");

            for (int j = 1; j < length / 2; j++) {
                System.out.println("Team " + teams.get(j - 1) + " : " + "Team " + teams.get(lengthList - j));
            }
            System.out.println("Team " + teams.get(length / 2 - 1) + " : " + "Team " + teams.get(lengthList));

            teams.add(0,""+teams.get(lengthList-1));
            teams.remove(length-1);

            System.out.println("---------------");

        }
    }
}