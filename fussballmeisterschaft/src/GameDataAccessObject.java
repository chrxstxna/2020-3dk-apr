import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameDataAccessObject {
    private final ChampionshipDataAccessObject dbAccess;
    public GameDataAccessObject(ChampionshipDataAccessObject dbAccess) {
        this.dbAccess = dbAccess;
    }


    public void insert(List<Game> games) {
        Connection connection = dbAccess.connect();
        String sql = "insert into game (home_team, away_team, score_home, score_away, game_date) VALUES (?, ?, null, null, ?)";
        try {
            for (Game g : games) {
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, g.getHomeTeam().getId());
                preparedStatement.setInt(2, g.getAwayTeam().getId());
                preparedStatement.setDate(3, g.getPlaydate());
                preparedStatement.execute();
                System.out.println("Game successfully inserted");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            dbAccess.close(connection);
        }
    }
}

