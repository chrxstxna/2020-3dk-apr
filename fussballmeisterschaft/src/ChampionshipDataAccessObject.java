import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ChampionshipDataAccessObject {
    private final String user;
    private final String password;
    private final String databaseURL;

    public ChampionshipDataAccessObject(Object host, Object port, Object database, Object user, Object password) {
        this.user = user.toString();
        this.password = password.toString();
        this.databaseURL = "jdbc:mariadb://"+host+":"+port+"/"+database;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }


   public Championship loadForLeague(String leagueName) {
       Connection connection = connect();
       String sql = "select c.id, l.id, l.name, l.year, t.id, t.name from championship c join league l on c.league = l.id join team t on c.team = t.id where l.name = '" + leagueName + "';";
       Statement select = null;
       try {
           select = connection.createStatement();
           ResultSet resultSet = select.executeQuery(sql);
           Championship championship = null;
           if (resultSet.next()) {
               League league = new League(resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4));
               championship = new Championship(resultSet.getInt(1), league);
               championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
           }
           while (resultSet.next()) {
               championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
           }
           return championship;
       } catch (SQLException throwables) {
           System.err.println("Could not run query!");
           throwables.printStackTrace();
       } finally {
           close(connection);
       }
       return null;
   }

    public Connection connect(){
        try {
            return DriverManager.getConnection(this.databaseURL, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
