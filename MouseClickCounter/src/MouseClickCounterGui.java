import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MouseClickCounterGui extends JFrame{
    public abstract class Counter implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent e) {
            int number = e.getClickCount();
        }
    }
    public MouseClickCounterGui(String title) {
        super(title);

        setLayout(new FlowLayout());

        JButton button = new JButton("How many clicks?");
        add(button);

        JLabel label = new JLabel();
        add(label);

        MouseListener mouseclick = new MouseAdapter() {
            private int click;
            @Override
            public void mouseClicked(MouseEvent e) {
                this.click ++;
                button.addActionListener(e1 -> label.setText(String.valueOf(click)));

            }
        };

        addMouseListener(mouseclick);
        setVisible(true);
        setSize(200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }


}
