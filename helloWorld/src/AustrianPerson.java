public class AustrianPerson extends Person{
    public AustrianPerson(String name) {
        super(name, "österreichisch");
        if(name.equals("Johann")){
            this.name = "Hauns";
        }
    }

    public String greet(){
        return "Servas " + this.name;
    }
}
