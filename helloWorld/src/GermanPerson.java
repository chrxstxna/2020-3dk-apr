public class GermanPerson extends Person{


    public GermanPerson(String name) {
        super(name, "deutsch");
        }

    public String greet(){
        return "Hallo " + this.name;
    }
}
