public class Englishperson extends Person {

    public Englishperson (String name) {
        super(name, "englisch");
    }

    @Override
    public String greet(){
        return "Hello " + this.name;
    }

}
