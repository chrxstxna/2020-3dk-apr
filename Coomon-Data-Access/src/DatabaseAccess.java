import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Supports Oracle and MariaDB Database
 */

public class DatabaseAccess {
    private String user;
    private String password;
    private String connectionUrl;

    public DatabaseAccess(String host, String port, String dbName,String user, String password, String dbVendor) throws DbVendorNotSuppertedException{
        this.user = user;
        this.password = password;
        this.connectionUrl = "jdbc:mariadb://"+host+":"+port+"/"+dbName;

        if ("mariadb".equals(dbVendor)) {
            initMariaDb(host,port,dbName);
        } else if("oracle".equals(dbVendor)){
            initOracleDb(host,port,dbName);
        }
        //throw new RuntimeException(dbVendor + " is currently not support. Use oracle or mariadb!");
        throw new DbVendorNotSuppertedException(dbVendor+" is not supported!");
    }


    private void initMariaDb(String host, String port, String dbName){
        this.connectionUrl = "jdbc:mariadb://"+host+":"+port+"/"+dbName;
        try{
            Class.forName("org.mariadb.jdbc.Driver");
        }catch (ClassNotFoundException e){
            throw new RuntimeException("Could not load databse driver", e);
        }
    }


    private void initOracleDb(String host, String port, String dbName){
        this.connectionUrl = "jdbc:oracle:thin:@"+host+":"+port+"/"+dbName;
        try{
            Class.forName("oracle.jdbc.OracleDriver");
        }catch (ClassNotFoundException e){
            throw new RuntimeException("Could not load databse driver", e);
        }
    }

    public Connection connect(){
        System.out.println("Connecting to database");
        try {
            return DriverManager.getConnection(this.connectionUrl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public void close(Connection connection){
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection"+throwables.getMessage());
        }
    }

}
