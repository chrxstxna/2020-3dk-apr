public class DbVendorNotSuppertedException extends Exception {

    public DbVendorNotSuppertedException(String message) {
        super(message);

    }
}
