import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class WindowClosingListener extends WindowAdapter {
    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Window closed now");
        System.exit(0);
    }
}
