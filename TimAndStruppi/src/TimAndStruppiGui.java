import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimAndStruppiGui extends JFrame {
    public TimAndStruppiGui(String title){
        super(title);

        setLayout(new FlowLayout());

        JButton tim = new JButton("Tim");
        tim.addActionListener(new ConsoleOutputActionListener());
        add(tim);

        JButton and = new JButton("and");
        and.addActionListener(new ConsoleOutputActionListener());
        add(and);

        JButton struppi = new JButton("Struppi");
        struppi.addActionListener(new ConsoleOutputActionListener());
        add(struppi);

        addWindowListener(new WindowClosingListener());
        setVisible(true);
        setSize(200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
