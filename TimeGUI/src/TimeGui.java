import javax.imageio.plugins.tiff.TIFFImageReadParam;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TimeGui extends JFrame {
    private JLabel timeLabel;
    private Color[] color = {Color.BLACK, Color.BLUE, Color.RED, Color.GREEN};
    private int currentColorPosition;

    public TimeGui(String title){
        super(title);
        setLayout(new FlowLayout());
        JPanel contentpane = new JPanel();
        setContentPane(contentpane);

        JButton farbeÄndern = new JButton("Farbewechsel starten");
        farbeÄndern.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Timer t = new Timer(3000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        currentColorPosition++;
                        if (currentColorPosition > color.length - 1) {
                            currentColorPosition = 0;
                        }
                        timeLabel.setForeground(color[currentColorPosition]);
                    }
                });
                t.start();
            }
        });
        contentpane.add(farbeÄndern);

        this.timeLabel = new JLabel(getTimeText());
        contentpane.add(timeLabel);

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTime();
            }
        });
        timer.start();

        setVisible(true);
        pack();
        setSize(300,80);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void setTime(){
        timeLabel.setText(getTimeText());
    }

    public String getTimeText(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss");
        return LocalTime.now().format(dateTimeFormatter);
    }
}
